#!/bin/bash

export DJANGO_SETTINGS_MODULE=settings
export PYTHONPATH=${PWD}

if [[ -n ${RABBITMQ_HOST} ]]; then
  export BROKER_DSN=`sed "s/127.0.0.1/${RABBITMQ_HOST}/" <<EOF
${BROKER_DSN}
EOF`
  export MANAGEMENT_DSN=`sed "s/127.0.0.1/${RABBITMQ_HOST}/" <<EOF
${MANAGEMENT_DSN}
EOF`
  echo Changed default rabbitmq server address by ${RABBITMQ_HOST}
  echo BROKER_DSN=$BROKER_DSN
  echo MANAGEMENT_DSN=$MANAGEMENT_DSN
fi

if [[ ${LOGTOFILE} == "YES" ]]; then
    echo uwsgi --ini ${PWD}/uwsgi.ini --daemonize /opt/logs/eo-dataflow-ui.log
    uwsgi --ini ${PWD}/uwsgi.ini --daemonize /opt/logs/eo-dataflow-ui.log
else
    echo uwsgi --ini ${PWD}/uwsgi.ini &
    uwsgi --ini ${PWD}/uwsgi.ini &
fi

echo exec "$@"
exec "$@"

