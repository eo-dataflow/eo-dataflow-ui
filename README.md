# Earth Observation dataflow UI

Web UI for monitoring and administering the eo-dataflow manager

# docker
## Build image
```
docker build -t eo-dataflow-ui:#1 .

with #1 version of docker image
```
## Run
```
docker run -p #1:8080 -e LOGTOFILE=#2 -e MANAGEMENT_DSN=#3 -e BROKER_DSN=#4 -v #5:/opt/logs eo-dataflow-ui:#6

with :
#1 published port number (no default)
#2 write uwsgi log to file YES or NO (default NO : write in sdtout)
#3 URL of rabbitMQ management (default https://rabbitmq-management-val.ifremer.fr:15672)
#4 URL of rabbitMQ for queue (default amqp://cersat-downloader-ui:Bruce and the Spatules@rabbitmq-cluster-val.ifremer.fr:5672/cersat-downloader)
#5 uwsgi log file mapping directory (in case LOGTOFILE=YES default /opt/logs in container)
#6 version of docker image
```
## Stop
```
stop conainer:
    docker container stop eo-dataflow-ui
stop and suppress container:  
    docker container rm -f eo-dataflow-ui
```

# Installation

## Conda

```
# Create conda env
conda env create -f environment.yaml

# Activate created env
conda activate eo-dataflow-ui

# Install the eo-dataflow-ui
./setup.py install
```

## Configuration
```
# Session database
django-admin migrate --settings conf.settings --pythonpath .
``` 
## Settings / run

set the MANAGEMENT_DSN and BROKER_DSN environment variables
```
vi conf/settings.py
vi conf/celery_cfg.json

vi conf/nginx.conf
vi conf/uwsgi.ini

conf/run
```

# Development

## Tooling

```
# Install developer tools
pip install -r requirements-dev.txt
```

### Linter

Using command-line:
```bash
# Run linter against current codebase
pylint -j8 --errors-only --output-format=colorized lib/
# The --errors-only flag only shows the errors

# Detect more issues:
pylint -j8 lib/
```

There are also plugins for many editors and IDEs.

### Code formatting

The code has been automatically formatted to follow PEP8 standard.
```bash
# Format every python source file in lib/ folder
autopep8 --in-place --recursive -j8 lib/
```

There are also plugins for many editors and IDEs.
