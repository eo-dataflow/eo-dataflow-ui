
from urllib.parse import urlsplit

import requests
import simplejson as json
# import ruamel.yaml as yaml
from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag
def get_download_id(values):
    if len(values) > 0:
        file_data = values[0]
        return file_data.attrib['id']
    else:
        return ""


@register.simple_tag
def get_type(values):
    file_data = values[0]
    return file_data.data_source.location.protocol


@register.simple_tag
def get_checked(variable, values):
    if variable in values:
        return '"' + values[0] + '" checked'
    else:
        return '"' + values[0] + '"'


@register.simple_tag
def get_source(values):
    loginpassword = False
    source = ""
    if len(values) > 0:
        file_data = values[0]

        protocol = str(file_data.data_source.location.protocol).capitalize()

        if protocol in ('Localpath', 'Localpointer', 'Localmove', 'Onlynotify'):
            source = file_data.data_source.location.rootpath
        else:
            if protocol == "Ftp":
                source = "ftp://"
            elif protocol == "Sftp":
                source = "sftp://"
            elif protocol == "Http":
                source = "http://"
            elif protocol.startswith('Https'):
                source = "https://"
            elif protocol == "Webdav":
                # source = "https://"
                source = "webdav://"
            if len(file_data.data_source.location.findall("login")) > 0:
                if file_data.data_source.location.login:
                    source += file_data.data_source.location.login
                    loginpassword = True
            if len(file_data.data_source.location.findall("password")) > 0:
                if file_data.data_source.location.password:
                    source += ':' + file_data.data_source.location.password
                    loginpassword = True
            if loginpassword:
                source += '@'
            source += file_data.data_source.location.server
            if len(file_data.data_source.location.findall("port")) > 0:
                if file_data.data_source.location.port is not None:
                    source += ':%s' % (str(file_data.data_source.location.port))
            source += '/' + file_data.data_source.location.rootpath

    return source


@register.simple_tag
def get_destination(values):

    if len(values) > 0:
        file_data = values[0]
        return file_data.data_destination.location
    else:
        return ""


def get_dict_protocol_options(location):

    if not hasattr(location, "protocol_option"):
        return {}
    protocol_options = str(location.protocol_option).replace('\n', '')
    options = '{' + protocol_options.strip("{ \n},") + '}'
    options = options.replace("'", '"')

    try:
        return json.loads(options)
        # return yaml.load(options, Loader=yaml.Loader)
    except (SyntaxError, ValueError, json.decoder.JSONDecodeError) as err:
        return {'Option Error !': err}


@register.simple_tag
def get_status_test(values):
    return get_synchro_var_value(values, 'State')


@register.simple_tag
def get_date_test(values):
    return get_synchro_var_value(values, 'Date')


@register.simple_tag
def get_status_synchro(values):
    return get_synchro_var_value(values, 'State')


@register.simple_tag
def get_date_synchro(values):
    return get_synchro_var_value(values, 'Date')


@register.simple_tag
def get_nb_file_synchro(values):
    return get_synchro_var_value(values, 'Nb_file')


def get_synchro_var_value(values, var):
    try:
        if len(values) == 2:
            map_test = values[1]
            return map_test[var]
        else:
            return ""
    except KeyError:
        return ""


@register.simple_tag
def get_output_file_synchro(values):
    return get_synchro_var_value(values, 'Output')


def get_synchro_var_value(values, var):
    try:
        if len(values) == 2:
            map_test = values[1]
            return map_test[var]
        else:
            return ""
    except KeyError:
        return ""


@register.simple_tag
def get_nb_file_reload(values, var):
    try:
        if len(values) == 3:
            map_test = values[2]
            return map_test['Nb_file']
        else:
            return ""
    except KeyError:
        return ""


@register.simple_tag(takes_context=True)
def get_queue_name(context):
    try:
        return context.request.session['active_queues'][context.request.session['queue']]
    except KeyError:
        return "?????????????"


@register.simple_tag(takes_context=True)
def get_active_queues(context):
    psswd = ""
    user = ""
    spliturl = urlsplit(settings.CELERY.conf['broker_url'])
    netloc = spliturl.netloc
    at = netloc.find('@')
    if at != -1:

        dpoint = netloc[:at].find(':')
        if dpoint == -1:
            user = netloc[:at]
        else:
            user = netloc[:dpoint]
            psswd = netloc[dpoint+1:at]

    host = settings.CELERY.conf['management_url']
    url_rmq = f"{host}/api/queues/{spliturl.path[1:]}"
    try:
        response = requests.get(url_rmq, auth=(user, psswd)).json()
    except Exception as err:
        select_queue = f'<option value="" selected = "selected"><b>*** Broker exception : ' \
                       f'{str(type(err))[8:-2]} *** </b></option>'
        return mark_safe(select_queue)
    if not isinstance(response, list):
        select_queue = f'<option value="" selected = "selected"><b>*** Broker error : ' \
                       f'{"unknown" if "error" not in response else response["error"]} ' \
                       f'***</b></option>'
        return mark_safe(select_queue)
    celery_queues = [q['name'] for q in response]
    downloader_queues = [(q['arguments']['alias'], q['name']) for q in response
                         if 'alias' in q['arguments']]
    active_queues = {downloader_queue[1]:downloader_queue[0] for downloader_queue in downloader_queues
                     if f'celery@{downloader_queue[1]}.celery.pidbox' in celery_queues}

    context.request.session['active_queues'] = active_queues
    select_queue = ''
    if 'queue' in context.request.session:
        selected_queue = context.request.session['queue']
    else:
        selected_queue = ''
    selected = False
    for queue, alias in active_queues.items():
        if queue == selected_queue:
            select_queue += f'<option value="{queue}" selected = "selected">{alias}</option>'
            selected = True
        else:
            select_queue += f'<option value="{queue}">{alias}</option>'

    if not selected:
        context.request.session['queue'] = ''
        if select_queue == '':
            select_queue = f'<option value="" selected = "selected"><b> * no eo-dataflow manager found * </b></option>'
        else:
            select_queue = f'<option value="" selected = "selected"><b> - select a eo-dataflow manager - </b></option>'\
                           + select_queue
    else:
        select_queue = f'<option value="" selected = "selected">- - - - - - - - - - - - - - - - </option>' \
                       + select_queue
    return mark_safe(select_queue)
