import os

from django.conf.urls import include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.contrib.auth import urls as auth_urls

from eo_dataflow_ui.apps.downloader_admin import urls as dladm_urls

admin.autodiscover()

url_prefix = os.environ.get("URL_PREFIX", "")

urlpatterns = [
    url(r"%sadmin" % url_prefix, include(admin.site.urls)),
    url(r"^%saccount/" % url_prefix, include(auth_urls)),
    url(r"%s" % url_prefix, include(dladm_urls, namespace="dladmin")),

]

urlpatterns += staticfiles_urlpatterns()
