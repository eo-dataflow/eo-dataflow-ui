from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.listdownload, name='listdownload'),
#    url(r'^test$', views.test, name='test'),
    url(r'^new$', views.new, name='new'),
    url(r'^nbActivatedFile$', views.getActivatedNbFile, name='nbActivatedFile'),
    url(r'^nbDesactivatedFile$', views.getDesactivatedNbFile, name='nbDeactivatedFile'),
    url(r'^nbTestFile$', views.getTestNbFile, name='nbTestFile'),
    url(r'^help$', views.help, name='help'),
    #url(r'^filterdownloads/(?P<pattern>\S+)$', views.filter_downloads, name='filterdownloads'),
    url(r'^createdownload/(?P<download_id>\S+)$', views.create_and_edit_download, name='createdownload'),

    url(r'^(?P<xml_filename>\S+)/(?P<download_id>\S+)/define_test_file$', views.define_test_file, name='define_test_file'),
    url(r'^(?P<xml_filename>\S+)/(?P<download_id>\S+)/save_test_file$', views.save_test_file, name='save_test_file'),
    url(r'^(?P<download_id>\S+)/testlog$', views.display_test_log, name='testlog'),
    url(r'^(?P<download_id>\S+)/synchrolog$', views.display_synchro_log, name='synchrolog'),
    url(r'^(?P<download_id>\S+)/logfile$', views.display_download_log, name='display_download_log'),

    url(r'^(?P<xml_filename>\S+)/(?P<download_id>\S+)/copy_as_file', views.copy_as_file, name='copy_as_file'),

    url(r'^change_queue$', views.new_queue, name='change_queue'),

    url(r'^(?P<xml_filename>\S+)/(?P<download_id>\S+)/reload_from_file', views.reload_from_file, name='reload_from_file'),

    url(r'^synchro/listing/download/(?P<download_id>\S+)', views.move_synchro_listing_to_download, name='move_synchro_listing_to_download'),
    url(r'^synchro/listing/(?P<xml_filename>\S+)/(?P<download_id>\S+)$', views.display_synchro_listing, name='display_synchro_listing'),
    url(r'^synchro/(?P<xml_filename>\S+)/(?P<download_id>\S+)$', views.status_synchro, name='status_synchro'),
    url(r'^test/(?P<xml_filename>\S+)/(?P<download_id>\S+)$', views.status_test, name='status_test'),

    url(r'^nbfile/(?P<xml_filename>\S+)/(?P<download_id>\S+)$', views.nb_file_synchro, name='nb_file_synchro'),
    url(r'^date/(?P<xml_filename>\S+)/(?P<download_id>\S+)$', views.date_synchro, name='date_synchro'),
    url(r'^checksynchro/(?P<xml_filename>\S+)/(?P<download_id>\S+)$', views.check_synchro, name='checksynchro'),
    #url(r'^runsynchro/(?P<xml_filename>\S+)/(?P<download_id>\S+)$', views.run_synchro, name='runsynchro'),

    url(r'^(?P<xml_filename>\S+)/save$', views.save, name='save'),

    url(r'^(?P<xml_filename>\S+)/activate$', views.activate, name='activate'),
    url(r'^(?P<xml_filename>\S+)/desactivate$', views.desactivate, name='deactivate'),
    url(r'^remove/(?P<xml_filename>\S+)/(?P<download_id>\S+)$', views.remove, name='remove'),
    url(r'^(?P<xml_filename>\S+)/delete$', views.delete, name='delete'),
    url(r'^(?P<xml_filename>\S+)/modetest$', views.modetest, name='modetest'),
    url(r'^(?P<xml_filename>\S+)/redownloaderror', views.redownload_error, name='redownload_error'),
    url(r'^(?P<xml_filename>\S+)/countdownloaderror', views.count_download_error, name='count_download_error'),

    url(r'^(?P<xml_filename>\S+)$', views.editdownload, name='edit'),
]



