
from eo_dataflow_ui.common.system.tasks import read_test_state_file, read_manual_listing, write_manual_listing


def add_test_status(queue, file_map):
    list_map_filename = file_map["TEST"]
    for map_filename in list_map_filename:
        for filename, values in list(map_filename.items()):
            mapFileName = values[0]
            map_test_status = {}
            download_id = mapFileName.attrib['id']

            map_test_status = read_test_status(queue, download_id)

            values.append(map_test_status)


def read_test_status(queue, download_id):
    map_test_status = {}

    try:
        task_result = read_test_state_file(queue, download_id + ".test")
        if task_result[0] and task_result[1]:
            lines = task_result[2].split("\n")

            for line in lines:
                line_split = line.split("=")
                if len(line_split) > 1:
                    map_test_status[line_split[0].strip()] = line_split[1].strip()
        else:
            map_test_status['Error'] = task_result[2]
    except Exception as error:
        map_test_status['Error'] = str(error)
    return map_test_status


class TestFile:
    def __init__(self, queue, download_id):
        self._id = download_id
        self._filename = "listing.list.test"
        self._queue = queue

    def get_content_test_file(self):

        content = read_manual_listing(self._queue, self._filename, self._id)
        filenames = content.split("\n")

        return filenames

    def save_content_test_file(self, test_filename, xml_filename):
        is_success = False
        is_success = write_manual_listing(self._queue, self._filename, self._id, test_filename)

        return is_success
