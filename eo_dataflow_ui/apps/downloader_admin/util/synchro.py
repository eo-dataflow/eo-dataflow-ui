
import datetime
from multiprocessing import Process
from eo_dataflow_ui.common.constants import SYNCHRO_HISTORY


from eo_dataflow_ui.common.downloaderUtil import get_workspace_dir_name
from eo_dataflow_ui.common.system.tasks import run_synchro, read_synchro_state_file, \
    copy_listing_file, check_status_synchro


def add_synchro_status(queue, file_map):
    list_map_filename = file_map["ON"]
    for map_filename in list_map_filename:

        for filename, values in list(map_filename.items()):
            if len(values) > 0:
                mapFileName = values[0]

                dl_id = mapFileName.attrib['id']
                task_result = read_synchro_status(queue, dl_id)
                values.append(task_result[2])


def get_synchro_state_file(id):
    synchro_state_name = "%s.synchro.job" % (id)
    return synchro_state_name


def read_synchro_status(queue, download_id):
    map_synchro_status = {}
    synchro_state_file = get_synchro_state_file(download_id)

    try:
        task_result = read_synchro_state_file(queue, synchro_state_file)
        if task_result[0] and task_result[1]:
            lines = task_result[2].split("\n")

            for line in lines:
                line_split = line.split("=")
                if len(line_split) > 1:
                    map_synchro_status[line_split[0].strip()] = line_split[1].strip()
        else:
            map_synchro_status['Error'] = task_result[2]
    except Exception as error:
        map_synchro_status['Error'] = str(error)
        task_result[0] = False
    return task_result[0], task_result[1], map_synchro_status


def move_synchro_listing_to_download(queue, download_id):
    task_result = read_synchro_status(queue, download_id)
    if task_result[0] and task_result[1]:
        result = copy_listing_file(queue,
                                   task_result[2]['Output'],
                                   get_workspace_dir_name(download_id))
        if result[0] and result[1]:
            return True
    return False


class Synchro(Process):
    def __init__(self, celery_queue, queue, xml_filename, download_id, modeDownload=False):
        super(Synchro, self).__init__()
        current_time = datetime.datetime.utcnow()
        #self.path= settings.CACHE_SYNCHRO_PATH + download_id + "/" + current_time.strftime('%Y/%m/%d')

        name = download_id + "_" + current_time.strftime('%Y%m%d_%H%M%S')
        synchro_output_filename = "%s.synchro" % (name)

        self.download_id = download_id
        self.modeDownload = modeDownload
        self.queue = queue
        self.celery_queue = celery_queue

        self.configuration_filename = xml_filename
        self.listing_filename = synchro_output_filename
        self.job_filename = download_id + ".synchro.job"
        self.synchro_subpath = get_workspace_dir_name(download_id) \
            + SYNCHRO_HISTORY + current_time.strftime('%Y%m%d')


    def check_status_synchro(self):
        return check_status_synchro(self.celery_queue, self.job_filename)

    def run(self):

        task_result = run_synchro(
            self.download_id,
            self.celery_queue,
            self.configuration_filename,
            self.listing_filename,
            self.job_filename,
            self.synchro_subpath
        )
        if task_result[0] and task_result[1]:
            self.queue.put(task_result[2])
        self.queue.put(task_result[2])

