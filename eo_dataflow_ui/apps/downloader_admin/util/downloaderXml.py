import time
import os
from lxml import etree, objectify
from lxml.etree import XMLSyntaxError
from django.utils.safestring import mark_safe
import eo_dataflow_ui.common.constants
from eo_dataflow_ui.common.system import tasks


def activate_download(queue, filename):

    name, fileExtension = os.path.splitext(filename)
    filesplit = name.split(".")
    if fileExtension == ".xml" and filesplit[-1] == "test":
        newname = (".").join(filesplit[:-1]) + fileExtension
    else:
        newname = os.path.splitext(filename)[0]

    task_result = tasks.rename_configuration_file(queue, filename, newname)
    if not task_result[0] or not task_result[1]:
        return False
    return task_result[2]


def delete_download(queue, filename):
    task_result = tasks.remove_configuration_file(queue, filename)
    if not task_result[0] or not task_result[1]:
        return False
    return task_result[2]


def mode_test(queue, filename):

    name, fileExtension = os.path.splitext(filename)
    if fileExtension == ".xml":
        newname = name + ".test" + fileExtension
    elif fileExtension in (".OFF", ".BAD"):
        filesplit = name.split(".")
        newname = (".").join(filesplit[:-1]) + ".test." + filesplit[-1]

    task_result = tasks.rename_configuration_file(queue, filename, newname)
    if not task_result[0] or not task_result[1]:
        return False
    return task_result[2]


def desactivate_download(queue, filename):

    name, fileExtension = os.path.splitext(filename)
    filesplit = name.split(".")
    if fileExtension == ".xml" and filesplit[-1] == "test":
        newname = (".").join(filesplit[:-1]) + '.xml.OFF'
    else:
        newname = filename + ".OFF"

    task_result = tasks.rename_configuration_file(queue, filename, newname)
    if not task_result[0] or not task_result[1]:
        return False
    return task_result[2]


def get_downloads_with_error(queue):

    return get_configurations_summaries_with_error(queue)


def get_configurations_summaries_with_error(queue):

    mapFile = {}
    mapFile["ON"] = []
    mapFile["OFF"] = []
    mapFile["TEST"] = []
    map_file_id = {}
    map_file_id_test = {}
    text_error = ""

    schema = get_schema(summary=True)
    parser = objectify.makeparser(strip_cdata=False, schema=schema)

    task_result = tasks.read_configuration_files_summary(queue)
    if not task_result[0] or not task_result[1]:
        return mapFile, map_file_id, map_file_id_test, mark_safe(task_result[2])

    configuration_files_summary = task_result[2]

    for filename in configuration_files_summary.keys():
        mapFileName = {}
        map = ""
        error = ""

        name, fileExtension = os.path.splitext(filename)
        filesplit = name.split(".")

        if fileExtension == ".xml" and filesplit[-1] == "download":
            map = "ON"
        elif fileExtension == ".xml" and filesplit[-1] == "test" and filesplit[-2] == "download":
            map = "TEST"
        elif fileExtension in (".OFF", ".BAD") and filesplit[-1] == "xml" and filesplit[-2] == "download":
            map = "OFF"
        elif filename[0:1] != '.':
            error = f"<b>{filename}</b> is not a configuration file"
        else:
            continue

        if configuration_files_summary[filename] == "":
            error = f"<b>{filename}</b> => read configuration error"
        elif error == "":
            try:

                file_data = objectify.fromstring(configuration_files_summary[filename], parser=parser)

                mapData = []
                mapData.append(file_data)
                mapFileName[filename] = mapData
                mapFile[map].append(mapFileName)

                if map == "ON":
                    map_file_id[filename] = file_data.attrib['id']
                if map == "TEST":
                    map_file_id_test[filename] = file_data.attrib['id']
            except XMLSyntaxError as e:
                error = f"<b>{filename}</b> => {str(e)}"

        if error != "":
            if text_error != "":
                text_error += "<br>"
            text_error += error

    return mapFile, map_file_id, map_file_id_test, mark_safe(text_error)


def get_downloads(queue):

    mapFile = {}
    mapFile["ON"] = []
    mapFile["OFF"] = []
    mapFile["TEST"] = []
    map_file_id = {}
    map_file_id_test = {}
    text_error = ''

    task_result = tasks.get_configuration_files(queue)
    if not task_result[0] or not task_result[1]:
        return mapFile, map_file_id, map_file_id_test, mark_safe(task_result[2])

    for filename in task_result[2]:
        mapFileName = {}
        if True:
            name, fileExtension = os.path.splitext(filename)
            filesplit = name.split('.')
            try:
                success, file_data = get_xml_data(queue, filename)
                mapData = []
                mapData.append(file_data)
                mapFileName[filename] = mapData

                if fileExtension == '.xml' and filesplit[-1] == 'download':
                    mapFile['ON'].append(mapFileName)
                    map_file_id[filename] = file_data.attrib['id']
                elif fileExtension == '.xml' and filesplit[-1] == 'test' and filesplit[-2] == 'download':
                    mapFile['TEST'].append(mapFileName)
                    map_file_id_test[filename] = file_data.attrib['id']
                elif fileExtension in ('.OFF', '.BAD') and filesplit[-1] == 'xml' and filesplit[-2] == 'download':
                    mapFile['OFF'].append(mapFileName)
            except XMLSyntaxError:
                pass

    return mapFile, map_file_id, map_file_id_test, mark_safe(text_error)


def get_schema(summary=False):
    if summary:
        fileschema = os.path.join(
            eo_dataflow_ui.common.constants.XSD_DIR, 'summary_download.xsd')
    else:
        fileschema = os.path.join(
            eo_dataflow_ui.common.constants.XSD_DIR, 'conf_download.xsd')
    schema = etree.XMLSchema(file=fileschema)
    return schema


def get_xml_data(queue, xml_file_name):

    task_result = tasks.read_configuration_file(queue, xml_file_name)
    if not task_result[0] or not task_result[1]:
        return False, task_result[2]

    schema = get_schema()
    parser = objectify.makeparser(strip_cdata=False, schema=schema)

    file_data = objectify.fromstring(task_result[2], parser=parser)

    if len(file_data.data_destination.findall("post-processing")) > 0:
        file_data.data_destination.findall("post-processing")[0].tag = \
            "post_processing"
    file_data.data_source.location.protocol = \
        str(file_data.data_source.location.protocol).capitalize()
    return True, file_data


def create_download(queue, xml_file_name):

    default_id = xml_file_name.split(".download.")[0]
    file_data = objectify.Element("download_config")
    file_data.attrib['id'] = default_id

    # Data source
    dummy = etree.SubElement(file_data, "data_source")
    dummy = etree.SubElement(file_data.data_source, "location")
    dummy = etree.SubElement(file_data.data_source.location, "protocol")
    file_data.data_source.location.protocol = "Localpath"
    dummy = etree.SubElement(file_data.data_source.location, "rootpath")
    file_data.data_source.location.rootpath = "."
    dummy = etree.SubElement(file_data.data_source, "date_extraction")
    file_data.data_source.date_extraction.attrib['plugin'] = 'RegexpDataReader'
    dummy = etree.SubElement(file_data.data_source.date_extraction, "regexp")
    file_data.data_source.date_extraction.regexp = "()"
    dummy = etree.SubElement(file_data.data_source.date_extraction, "format")
    dummy = etree.SubElement(file_data.data_source, "selection")
    dummy = etree.SubElement(file_data.data_source.selection, "update")
    file_data.data_source.selection.update = "modified"

    # Data Destination
    dummy = etree.SubElement(file_data, "data_destination")
    dummy = etree.SubElement(file_data.data_destination, "location")
    file_data.data_destination.location = "."
    dummy = etree.SubElement(file_data.data_destination, "organization")
    dummy = etree.SubElement(file_data.data_destination.organization, "type")
    file_data.data_destination.organization.type = "spool"

    # Download Settings
    dummy = etree.SubElement(file_data, "download_settings")
    dummy = etree.SubElement(file_data.download_settings, "database")
    dummy = etree.SubElement(
        file_data.download_settings.database, "keep_last_scan")
    file_data.download_settings.database.keep_last_scan = "true"
    dummy = etree.SubElement(file_data.download_settings,
                             "check_source_availability")
    file_data.download_settings.check_source_availability = "true"
    dummy = etree.SubElement(file_data.download_settings, "monitoring")
    file_data.download_settings.monitoring = "true"

    # validation
    objectify.deannotate(file_data, cleanup_namespaces=True)
    xml_string = etree.tostring(file_data, pretty_print=True)
    valid_xml(xml_string)

    result = save_download(queue, xml_file_name, xml_string)
    if not isinstance(result, bool):
        return False, result
    return True, result


def copy_as_file(queue, src, dst, download_id_dst):

    task_result = tasks.copy_configuration_file(queue, src, dst)
    if not task_result[0] or not task_result[1]:
        return False

    time.sleep(0.5)
    task_result = tasks.exists_configuration_file(queue, dst)
    if not task_result[0] or not task_result[1]:
        return False

    success, file_data = get_xml_data(queue, dst)
    if file_data is None:
        return False

    file_data.attrib['id'] = download_id_dst
    # replace tag post_processing by post-processing
    if len(file_data.data_destination.findall("post_processing")) > 0:
        file_data.data_destination.findall("post_processing")[0].tag =\
            "post-processing"

    objectify.deannotate(file_data, cleanup_namespaces=True)
    xml_string = etree.tostring(file_data, pretty_print=True)
    valid_xml(xml_string)

    return save_download(queue, dst, xml_string)

"""
# Save data submitted by the user.
"""


def save_download(queue, xml_file_name, xml_string):
    result_task = tasks.write_configuration_file(queue, xml_file_name, xml_string)
    """
    if result_task[0] and result_task[1]:
        return result_task[2]
    return False
    """
    if not result_task[0] or not result_task[1]:
        return result_task[2]
    return True


def set_download_config(queue, xml_file_name, submitted_data):

    file_data = objectify.Element("download_config")
    file_data.attrib['id'] = submitted_data['id']

    if 'debug_enabled' in submitted_data and submitted_data['debug_enabled']:
        dummy = etree.SubElement(file_data, "debug")
        file_data.debug = True

    dummy = etree.SubElement(file_data, "data_source")
    save_data_source(file_data, submitted_data)
    dummy = etree.SubElement(file_data, "data_destination")
    save_data_destination(file_data, submitted_data)
    dummy = etree.SubElement(file_data, "download_settings")
    save_download_settings(file_data, submitted_data)

    # replace tag post_processing by post-processing
    if len(file_data.data_destination.findall("post_processing")) > 0:
        file_data.data_destination.findall("post_processing")[0].tag =\
            "post-processing"

    objectify.deannotate(file_data, cleanup_namespaces=True)
    xmlString = etree.tostring(file_data, pretty_print=True)
    # validation xsd
    return xmlString, file_data,  # map_DCheck_config


def valid_xml(xml_string):
    schema = get_schema()
    parser = etree.XMLParser(schema=schema)
    root = etree.fromstring(xml_string, parser)


def save_data_source(file_data, submitted_data):
    dummy = etree.SubElement(file_data.data_source, "location")
    parent_loc = file_data.data_source.location

    dummy = etree.SubElement(parent_loc,  "protocol")
    parent_loc.protocol = submitted_data['type_remote']

    if not str(parent_loc.protocol).startswith('Local'):
        dummy = etree.SubElement(parent_loc, "server")
        parent_loc.server = submitted_data['server_remote']
        if submitted_data['port_remote']:
            dummy = etree.SubElement(parent_loc, "port")
            parent_loc.port = submitted_data['port_remote']
        dummy = etree.SubElement(parent_loc, "login")
        if submitted_data['user_remote']:
            parent_loc.login = submitted_data['user_remote']
        dummy = etree.SubElement(parent_loc, "password")
        if submitted_data['passwd_remote']:
            parent_loc.password = submitted_data['passwd_remote']
        protocol_options = ""
        for key, value in submitted_data.dict().items():
            if key.startswith('protocol_option_') and value:
                protocol_options += "'" + key[len("protocol_option_"):] + "': '" + value + "',"
        if protocol_options != "":
            dummy = etree.SubElement(parent_loc, "protocol_option")
            parent_loc.protocol_option = protocol_options[:-1]
    if submitted_data['repos_remote']:
        dummy = etree.SubElement(parent_loc, "rootpath")
        parent_loc.rootpath = submitted_data['repos_remote']

    # date_extraction
    dummy = etree.SubElement(file_data.data_source, "date_extraction")
    parent_de = file_data.data_source.date_extraction
    parent_de.attrib['plugin'] = submitted_data['data_reader']
    dummy = etree.SubElement(parent_de, "regexp")
    dummy = etree.SubElement(parent_de, "format")
    if submitted_data['data_reader'] not in  ('GribDataReader', 'TreeDataReader'):
        if submitted_data['data_reader'] != "NetCDFDataReader":
            parent_de.regexp = submitted_data['regexp_date']
            parent_de.format = submitted_data['date_format']
        else:
            parent_de.regexp = submitted_data['regexp_date2']
            parent_de.format = submitted_data['date_format2']

    # selection
    dummy = etree.SubElement(file_data.data_source, "selection")
    parent_sel = file_data.data_source.selection
    dummy = etree.SubElement(parent_sel, "update")
    if 'redownload' in submitted_data:
        parent_sel.update = "modified"
    else:
        parent_sel.update = "new"

    # date_folders
    if parent_loc.protocol != "Https_opensearch" \
            and (submitted_data['directories_pattern']
                 or submitted_data['directories_delta']
                 or submitted_data['directories_maxdate']
                 or submitted_data['directories_mindate']):
        dummy = etree.SubElement(parent_sel, "date_folders")
        dummy = etree.SubElement(parent_sel.date_folders, "pattern")
        parent_sel.date_folders.pattern = submitted_data['directories_pattern']
        dummy = etree.SubElement(parent_sel.date_folders, "backlog_in_days")
        parent_sel.date_folders.backlog_in_days = submitted_data['directories_delta']
        dummy = etree.SubElement(parent_sel.date_folders, "max_date")
        parent_sel.date_folders.max_date = submitted_data['directories_maxdate']
        dummy = etree.SubElement(parent_sel.date_folders, "min_date")
        parent_sel.date_folders.min_date = submitted_data['directories_mindate']

    # directories
    if parent_loc.protocol != "Https_opensearch" \
            and (submitted_data['ignore_directories_newer_than']
                 or submitted_data['ignore_directories_older_than']
                 or submitted_data['ignore_directories_regexp']
                 or submitted_data['include_directories_regexp']):
        dummy = etree.SubElement(parent_sel, "directories")
        dummy = etree.SubElement(parent_sel.directories, "ignore_newer_than")
        parent_sel.directories.ignore_newer_than = submitted_data['ignore_directories_newer_than']
        dummy = etree.SubElement(parent_sel.directories, "ignore_modify_time_older_than")
        parent_sel.directories.ignore_modify_time_older_than = submitted_data['ignore_directories_older_than']
        dummy = etree.SubElement(parent_sel.directories, "ignore_regexp")
        parent_sel.directories.ignore_regexp = submitted_data['ignore_directories_regexp']
        dummy = etree.SubElement(parent_sel.directories, "regexp")
        parent_sel.directories.regexp = submitted_data['include_directories_regexp']

    # files
    if submitted_data['ignore_files_modify_older_than'] != "" \
            or submitted_data['ignore_files_regexp'] != "" \
            or submitted_data['include_files_regexp'] != "":
        dummy = etree.SubElement(parent_sel, "files")
        dummy = etree.SubElement(parent_sel.files, "ignore_modify_time_older_than")
        parent_sel.files.ignore_modify_time_older_than = submitted_data['ignore_files_modify_older_than']
        dummy = etree.SubElement(parent_sel.files, "ignore_regexp")
        parent_sel.files.ignore_regexp = submitted_data['ignore_files_regexp']
        dummy = etree.SubElement(parent_sel.files, "regexp")
        parent_sel.files.regexp = submitted_data['include_files_regexp']

    if parent_loc.protocol == "Https_opensearch":
        dummy = etree.SubElement(parent_sel, "opensearch")
        dummy = etree.SubElement(parent_sel.opensearch, "dataset")
        parent_sel.opensearch.dataset = submitted_data['opensearch_dataset']
        dummy = etree.SubElement(parent_sel.opensearch, "area")
        parent_sel.opensearch.area = submitted_data['opensearch_area']
        if 'opensearch_format' in submitted_data:
            dummy = etree.SubElement(parent_sel.opensearch, "request_format")
            parent_sel.opensearch.request_format = submitted_data['opensearch_format']

    if parent_loc.protocol in ('Https_eop_os', 'Https_xx_daac', 'https_po_daac') \
            and 'geotemporal_list' in submitted_data:
        dummy = etree.SubElement(parent_sel, "geotemporal")
        dummy = etree.SubElement(parent_sel.geotemporal, "geotemporal_list")
        parent_sel.geotemporal.geotemporal_list = submitted_data['geotemporal_list']

def save_data_destination(file_data, submitted_data):
    parent = file_data.data_destination

    # location
    for key, value in submitted_data.dict().items():
        if key.startswith('repos_local__') and value:
            location = etree.Element('location')
            location.text = value
            parent.append(location)

    # optional_spool_location
    for key, value in submitted_data.dict().items():
        if key.startswith('spool_location__') and value:
            spool_location = etree.Element('optional_spool_location')
            spool_location.text = value
            parent.append(spool_location)

    # organization
    dummy = etree.SubElement(parent, "organization")
    parent_org = parent.organization
    if submitted_data['repos_local_tree']:
        dummy = etree.SubElement(parent_org, "subpath")
        parent_org.subpath = submitted_data['repos_local_tree']
    dummy = etree.SubElement(parent_org, "type")
    parent_org.type = submitted_data['type_local']
    dummy = etree.SubElement(parent_org, "keep_parent_folder")
    if 'repos_local_SAFE' in submitted_data \
            and file_data.data_source.date_extraction.attrib['plugin'] == "DirRegexpDataReader":
        parent_org.keep_parent_folder = "true"
    else:
        parent_org.keep_parent_folder = "false"

    # post_processing
    dummy = etree.SubElement(file_data.data_destination, "post_processing")
    parent_pp = parent.post_processing
    if submitted_data['checksum'] != "none" and submitted_data['checksum'] is not None:
        dummy = etree.SubElement(parent_pp, "checksum")
        parent_pp.checksum = submitted_data['checksum']
    if submitted_data['compression_type'] != "none" and submitted_data['compression_type'] is not None:
        dummy = etree.SubElement(parent_pp, "compression")
        if submitted_data['compression_type'] != 'none':
            parent_pp.compression = 'uncompress'
        else:
            parent_pp.compression = 'none'
        parent_pp.compression.attrib['type'] = submitted_data['compression_type']
        if 'compressionSubDir' in submitted_data:
            parent_pp.compression.attrib['subdir'] = "true"
        else:
            parent_pp.compression.attrib['subdir'] = "false"
    if submitted_data['extraction_type'] != "none" and submitted_data['extraction_type'] is not None:
        dummy = etree.SubElement(parent_pp, "archive")
        parent_pp.archive = submitted_data['extraction_type']
        if 'extractionSubDir' in submitted_data:
            parent_pp.archive.attrib['subdir'] = "true"
        else:
            parent_pp.archive.attrib['subdir'] = "false"
    if submitted_data['file_group_name'] != "":
        dummy = etree.SubElement(parent_pp, "file_group_name")
        parent_pp.file_group_name = submitted_data['file_group_name']

    # spool_link
    for key, value in submitted_data.dict().items():
        if key.startswith('spool_link__') and value:
            spool_link = etree.Element('spool_link')
            spool_link.text = value
            parent_pp.append(spool_link)

    if submitted_data['validation'] != "none" and submitted_data['validation'] is not None:
        dummy = etree.SubElement(parent_pp, "validation")
        parent_pp.validation = submitted_data['validation']

    # files_filter
    # files
    if submitted_data['ignore_files_pp_regexp'] != "" \
            or submitted_data['include_files_pp_regexp'] != "":
        dummy = etree.SubElement(parent_pp, "files_filter")
        dummy = etree.SubElement(parent_pp.files_filter, "ignore_regexp")
        parent_pp.files_filter.ignore_regexp = submitted_data['ignore_files_pp_regexp']
        dummy = etree.SubElement(parent_pp.files_filter, "regexp")
        parent_pp.files_filter.regexp = submitted_data['include_files_pp_regexp']


    file_data.append(parent)


def save_download_settings(file_data, submitted_data):

    parent = file_data.download_settings

    # database
    dummy = etree.SubElement(parent, "database")
    parent_db = parent.database
    if submitted_data['purge_scan_older_than'] != "":
        dummy = etree.SubElement(parent_db, "purge_scans_older_than")
        parent_db.purge_scans_older_than = submitted_data['purge_scan_older_than']
    dummy = etree.SubElement(parent_db, "keep_last_scan")
    if 'keep_last_scan' in submitted_data:
        parent_db.keep_last_scan = "true"
    else:
        parent_db.keep_last_scan = "false"

    if submitted_data['loop_delay'] != "":
        dummy = etree.SubElement(parent, "cycle_length")
        parent.cycle_length = submitted_data['loop_delay']

    if submitted_data['max_activated_files_by_loop'] != "":
        dummy = etree.SubElement(parent, "max_nb_of_files_downloaded_per_cycle")
        parent.max_nb_of_files_downloaded_per_cycle = submitted_data['max_activated_files_by_loop']

    if submitted_data['parallel'] != "":
        dummy = etree.SubElement(parent, "nb_parallel_downloads")
        parent.nb_parallel_downloads = submitted_data['parallel']

    if submitted_data['nb_retry'] != "":
        dummy = etree.SubElement(parent, "nb_retries")
        parent.nb_retries = submitted_data['nb_retry']

    if submitted_data['wait_between_downloads'] != "":
        dummy = etree.SubElement(parent, "delay_between_downloads")
        parent.delay_between_downloads = submitted_data['wait_between_downloads']

    if submitted_data['wait_between_scan'] != "":
        dummy = etree.SubElement(parent, "delay_between_scans")
        parent.delay_between_scans = submitted_data['wait_between_scan']

    if submitted_data['to_download_max_nbr'] != "":
        dummy = etree.SubElement(parent, "max_nb_of_lines_in_auto_listing")
        parent.max_nb_of_lines_in_auto_listing = submitted_data['to_download_max_nbr']

    if submitted_data['wait_before_download_delay'] != "":
        dummy = etree.SubElement(parent, "delay_before_download_start")
        parent.delay_before_download_start = submitted_data['wait_before_download_delay']

    dummy = etree.SubElement(parent, "check_source_availability")
    if 'check_provider_before_download' in submitted_data:
        parent.check_source_availability = "true"
    else:
        parent.check_source_availability = "false"

    dummy = etree.SubElement(parent, "monitoring")
    if 'monitoring_enabled' in submitted_data:
        parent.monitoring = "true"
    else:
        parent.monitoring = "false"

    if submitted_data['project_name'] != "":
        dummy = etree.SubElement(parent, "project_name")
        parent.project_name = submitted_data['project_name']

    if submitted_data['protocol_timeout'] != "":
        dummy = etree.SubElement(parent, "protocol_timeout")
        parent.protocol_timeout = submitted_data['protocol_timeout']
