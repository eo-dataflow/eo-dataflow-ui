#
# -*- coding: UTF-8 -*-
#
"""XML File writer tools encapsulation
"""

__docformat__ = 'epytext'


from xml.etree import ElementTree
from eo_dataflow_ui.common.system.tasks import write_content_file


class XMLWriter(object):
    """
    Class XMLWriter
    """

    def __init__(self):
        """
        XMLWriter initialization
        """
        pass

    def write(self, xmlTree, file):
        is_success = False
        try:
            root = xmlTree.getroot()
            self.__indent(root, 0)
            ElementTree.dump(root)
            xmlString = ElementTree.tostring(root, encoding='utf-8')
            is_success = write_content_file(file, xmlString)
            return is_success

        except Exception:
            #raise "Error during xml writing (file : %s)" % file
            raise

    def __indent(self, elem, level):
        indentStr = ["\n"]
        for index in range(level):
            indentStr.append(" ")

        i = ''.join(indentStr)

        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + " "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.__indent(elem, level + 1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def newXmlTree(self):
        return ElementTree.ElementTree()

    def xmlTreeSetRoot(self, tree, root):
        tree._setroot(root)

    def newNode(self, tag, text, tail="", attribute={}):
        e = ElementTree.Element(tag, attrib=attribute)
        e.text = text
        e.tail = tail
        return e

    def newSubNode(self, node, tag, text, tail="", attribute={}):
        e = ElementTree.SubElement(node, tag, attrib=attribute)
        e.text = text
        e.tail = tail
        return e

    def nodeSetText(self, node, text):
        node.text = text

    def nodeAddAttrib(self, node, key, value):
        node.set(key, value)
