import datetime
import json
import os
import time
import re

from multiprocessing import Queue
from lxml.etree import XMLSyntaxError

# from django.core.urlresolvers import reverse
from django.urls import reverse
from django.core.files.storage import FileSystemStorage
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.template import RequestContext
# from django.utils.translation import ugettext as _
from django.utils.translation import gettext as _

from eo_dataflow_ui.common.constants import LOGGER_DEFAULT_FILE_NAME, LOGGER_SYNCHRO_FILE_NAME, \
                                               EXTENSION_DISABLED, EXTENSION_BAD, EXTENSION_TEST, EXTENSION_ACTIVE
from eo_dataflow_ui.common.downloaderUtil import get_workspace_dir_name
from eo_dataflow_ui.apps.downloader_admin.util import downloaderXml, synchro, testFile
from eo_dataflow_ui.apps.downloader_admin.util.synchro import Synchro
from eo_dataflow_ui.apps.downloader_admin.util.testFile import TestFile
from eo_dataflow_ui.common.system import tasks

from eo_dataflow_ui.templatetags.xml_data_tags import get_dict_protocol_options


def activate(request, xml_filename):
    is_success = downloaderXml.activate_download(request.session['queue'], xml_filename)
    file_map, map_file_id, map_file_id_test, text_error = \
        downloaderXml.get_downloads_with_error(request.session['queue'])
    if is_success:
        return render(
            request,
            'adminview/activateddownload.html',
            {'file_map': file_map, 'map_file_id': map_file_id, "tabSelected": "activated",
             'event': "filemovetoactivated", "xml_file_name": xml_filename, },
        )
    else:
        return render(
            request,
            'adminview/activateddownload.html',
            {'file_map': file_map, 'map_file_id': map_file_id,
             'error': 'activate_error', "xml_file_name": xml_filename, },
        )


def desactivate(request, xml_filename):
    is_success = downloaderXml.desactivate_download(request.session['queue'], xml_filename)
    file_map, map_file_id, map_file_id_test, text_error = \
        downloaderXml.get_downloads_with_error(request.session['queue'])
    if is_success:
        return render(
            request,
            'adminview/activateddownload.html',
            {'file_map': file_map, 'map_file_id': map_file_id, "tabSelected": "desactivated",
             'event': "filemovetodesactivated", "xml_file_name": xml_filename, },
        )
    else:
        return render(
            request,
            'adminview/activateddownload.html',
            {'file_map': file_map, 'map_file_id': map_file_id,
             'error': 'desactivate_error', "xml_file_name": xml_filename, },
        )


def modetest(request, xml_filename):
    is_success = downloaderXml.mode_test(request.session['queue'], xml_filename)
    file_map, map_file_id, map_file_id_test, text_error = \
        downloaderXml.get_downloads_with_error(request.session['queue'])
    if is_success:
        return render(
            request,
            'adminview/activateddownload.html',
            {'file_map': file_map, 'map_file_id': map_file_id, 'tabSelected': 'test',
             'event': 'filemovetotest', 'xml_file_name': xml_filename},
        )
    else:
        return render(
            request,
            'adminview/activateddownload.html',
            {'file_map': file_map, 'map_file_id': map_file_id,
             'error': 'test_error', 'xml_file_name': xml_filename},
        )


def delete(request, xml_filename):
    success, file_data = downloaderXml.get_xml_data(request.session['queue'], xml_filename)
    if not success:
        pass
    else:
        download_id = file_data.attrib['id']
        remove(request, xml_filename, download_id)


def remove(request, xml_filename, download_id):
    date = datetime.datetime.utcnow().strftime('%Y%m%d_%H%M%S')
    result_task = tasks.archive_download(request.session['queue'],
                                         xml_filename, get_workspace_dir_name(download_id), "." + date + ".old")
    error = ""
    if not result_task[0] or not result_task[1]:
        error = result_task[2]
    return HttpResponse(error)


def listdownload(request):
    queue = request.session['queue'] if 'queue' in request.session else ""
    server_error = ''
    if queue == '':
        file_map, map_file_id, map_file_id_test = {}, {}, {}
        text_error = ""
    else:
        file_map, map_file_id, map_file_id_test, text_error = downloaderXml.get_downloads_with_error(queue)
        if text_error.startswith('server_error'):
            server_error = text_error[len('server_error') + 1:]
            text_error = 'server_error'
        # TODO: add download status not synchro status ?
        # synchro.add_synchro_status(queue, file_map)
    return render(
        request,
        'adminview/activateddownload.html',
        {'file_map': file_map, 'map_file_id': map_file_id, 'map_file_id_test': map_file_id_test,
         'error': text_error, 'file_name': '', 'server_error': server_error,
         },
    )


def display_download_log(request, download_id):
    xml_filename = download_id + EXTENSION_ACTIVE
    return render(
        request,
        'adminview/viewlog.html',
        {'download_id': download_id, 'xml_filename': xml_filename}
    )


def getActivatedNbFile(request):
    file_map, map_file_id, map_file_id_test, task_error = \
        downloaderXml.get_downloads_with_error(request.session['queue'])
    return HttpResponse(len(file_map['ON']))


def getDesactivatedNbFile(request):
    file_map, map_file_id, map_file_id_test, task_error = \
        downloaderXml.get_downloads_with_error(request.session['queue'])
    return HttpResponse(len(file_map['OFF']))


def getTestNbFile(request):
    file_map, map_file_id, map_file_id_test, task_error = \
        downloaderXml.get_downloads_with_error(request.session['queue'])
    return HttpResponse(len(file_map['TEST']))


def editdownload(request, xml_filename):
    if 'queue' not in request.session or request.session['queue'] == '':
        return render(
            request,
            'adminview/activateddownload.html',
            {'file_map': None, 'map_file_id': None, 'error': None, 'file_name': None, },
        )

    try:
        success, file_data = downloaderXml.get_xml_data(request.session['queue'], xml_filename)
        if success:
            protocol_options = get_dict_protocol_options(file_data.data_source.location)
            return render(
                request,
                'adminview/editdownload.html',
                {'file_data': file_data, 'file_name': xml_filename, 'error': "", 'protocol_options': protocol_options},
            )
        error = file_data
    except Exception as err:
        error = str(err)

    file_map, map_file_id, map_file_id_test, text_error = \
        downloaderXml.get_configurations_summaries_with_error(request.session['queue'])
    return render(
        request,
        'adminview/activateddownload.html',
        {'file_map': file_map, 'map_file_id': map_file_id, 'error': error, 'file_name': xml_filename, },
        )


def new(request):
    error = ""
    tab_selected = "add"
    file_map = {}
    map_file_id = {}
    xml_file_name_disabled = ""
    event = "x"
    task_error = ""

    queue = request.session['queue'] if 'queue' in request.session else ''
    if queue != '':
        new_file = request.POST['newfile']
        xml_file_name_disabled = new_file + EXTENSION_DISABLED
        xml_file_name_bad = new_file + EXTENSION_BAD
        xml_file_name_test = new_file + EXTENSION_TEST
        xml_file_name_activated = new_file + EXTENSION_ACTIVE

        if not new_file:
            error = "Error: download name must be defined"

        elif not re.search('^[A-Za-z0-9_-]+$', new_file):
            error = 'Error: only the characters A-Z, a-z, 0-9, _ and - are allowed'

        if error == '':
            result_task = tasks.exists_configuration_files(queue,
                                                           (xml_file_name_activated,
                                                            xml_file_name_bad,
                                                            xml_file_name_disabled,
                                                            xml_file_name_test))
            if result_task[0] and result_task[1] and not result_task[2]:
                result = tasks.exists_configuration_id(request.session['queue'],
                                                       new_file)
                if not result[0] or not result[1]:
                    error = result[2]
                elif result[2]:
                    error = "Error: download id already exists"
            elif not result_task[0] or not result_task[1]:
                error = result_task[2]
            else:
                error = "Error: download filename already exists"

        if error == '':
            result_task = downloaderXml.create_download(queue, xml_file_name_disabled)
            if not result_task[0] or not result_task[1]:
                error = result_task[2]
            else:
                xml_file_name_disabled = new_file + EXTENSION_DISABLED
                tab_selected = "desactivated"
                event = 'filecreated'

        file_map, map_file_id, map_file_id_test, task_error = downloaderXml.get_downloads_with_error(queue)

    else:
        error = 'Error: no selected downloader'

    if error == '':
        return HttpResponseRedirect(reverse('dladmin:edit', args=(xml_file_name_disabled,)))

    return render(
        request,
        'adminview/activateddownload.html',
        {'file_map': file_map, 'map_file_id': map_file_id,
         'tabSelected': tab_selected, 'event': event, 'xml_file_name': xml_file_name_disabled,
         'file_error': error, 'file_name': '', 'new_filename': new_file,
         'error': task_error},)


def save(request, xml_filename):
    xml_string, file_data = \
        downloaderXml.set_download_config(request.session['queue'], xml_filename, request.POST)
    try:
        downloaderXml.valid_xml(xml_string)
    except XMLSyntaxError as e:
        error = e
        return render(
            request,
            'adminview/editdownload.html',
            {'file_data': file_data[0], 'file_name': xml_filename, 'error': error},
        )

    is_success = downloaderXml.save_download(request.session['queue'], xml_filename, xml_string)
    if is_success:
        return HttpResponseRedirect(reverse('dladmin:listdownload'))
    else:
        return render(
            request,
            'adminview/editdownload.html',
            {'file_data': file_data[0], 'file_name': xml_filename, 'error': 'save_error'},
        )


def define_test_file(request, xml_filename, download_id):
    test_file = TestFile(request.session['queue'], download_id)
    test_file_content = test_file.get_content_test_file()
    return HttpResponse(test_file_content)


def save_test_file(request, xml_filename, download_id):
    test_filename = request.POST['filename']
    test_file = TestFile(request.session['queue'], download_id)
    test_file.save_content_test_file(test_filename, xml_filename)

    map_test_status = testFile.read_test_status(request.session['queue'], download_id)
    return HttpResponse(map_test_status['State'])


def status_test(request, xml_filename, download_id):
    map_test_status = testFile.read_test_status(request.session['queue'], download_id)
    return HttpResponse(map_test_status['State'])


def display_test_log(request, download_id):
    task_result = tasks.read_log_file(request.session['queue'],
                                      LOGGER_DEFAULT_FILE_NAME + download_id + '_test.log')
    RequestContext(request, {'data_file': task_result[2]})
    return HttpResponse(task_result[2])


def check_synchro(request, xml_filename, download_id):
    text_error = ""
    queue = Queue()
    synchro_process = Synchro(request.session['queue'], queue, xml_filename, download_id, False)

    task_result = synchro_process.check_status_synchro()
    if not task_result[0] or not task_result[1]:
        text_error = task_result[2]
    elif task_result[2] == "stop":
        synchro_process.start()
        time.sleep(2)
        result = synchro_process.queue.get()
        if isinstance(result, str):
            text_error = "synchro_not_started:" + result
    elif task_result[2] == "start":
        text_error = "synchro_running"
    else:
        text_error = "unkown_synchro_state"
    return HttpResponse(text_error)


def status_synchro(request, xml_filename, download_id):
    task_result = synchro.read_synchro_status(request.session['queue'], download_id)
    if task_result[0] and task_result[1]:
        if 'State' in task_result[2]:
            return HttpResponse(task_result[2]['State'])
        else:
            HttpResponse('')
    return HttpResponse(task_result[2])


def nb_file_synchro(request, xml_filename, download_id):
    task_result = synchro.read_synchro_status(request.session['queue'], download_id)
    if task_result[0] and task_result[1]:
        if 'Nb_file' in task_result[2]:
            return HttpResponse(task_result[2]['Nb_file'])
        else:
            HttpResponse('')
    return HttpResponse(task_result[2])


def date_synchro(request, xml_filename, download_id):
    task_result = synchro.read_synchro_status(request.session['queue'], download_id)
    if task_result[0] and task_result[1]:
        if 'Date' in task_result[2]:
            return HttpResponse(task_result[2]['Date'])
        else:
            HttpResponse('')
    return HttpResponse(task_result[2])


def move_synchro_listing_to_download(request, download_id):
    error = ""
    is_success = synchro.move_synchro_listing_to_download(request.session['queue'], download_id)
    if not is_success:
        error = "error"
    return HttpResponse(error)


def display_synchro_listing(request, xml_filename, download_id):
    task_result = synchro.read_synchro_status(request.session['queue'], download_id)
    if task_result[0] and task_result[1]:
        if 'Output' in task_result[2]:
            listing_file = task_result[2]['Output']
            task_result1 = tasks.read_listing_file(request.session['queue'], listing_file)
            if task_result1[0] and task_result1[1]:
                kwvars = {'listing_list': task_result1[2], 'download_id': download_id, 'xml_filename': xml_filename}
                return render(
                    request,
                    'adminview/viewfile.html',
                    kwvars
                )
            return HttpResponse(task_result1[2])
        else:
            return HttpResponse('No listing available')

    else:
        return HttpResponse(task_result[2])


def display_synchro_log(request, download_id):
    task_result = tasks.read_log_file(request.session['queue'],
                                      LOGGER_SYNCHRO_FILE_NAME + download_id + '.log')
    RequestContext(request, {'log_data': task_result[2]})
    return HttpResponse(task_result[2])


def redownload_error(request, xml_filename):
    tesk_result = tasks.redownload_error(request.session['queue'], xml_filename)
    return HttpResponse(tesk_result[2])


def count_download_error(request, xml_filename):
    result_task = tasks.count_download_error(request.session['queue'], xml_filename)
    return HttpResponse(result_task[2])


def reload_from_file(request, xml_filename, download_id):
    data = ""
    result = True
    try:
        data_filename = request.FILES['data_filename']
        fs = FileSystemStorage()

        filename = fs.save(data_filename.name, data_filename)

        with fs.open(filename, "r+") as f:
            for chunk in f.chunks():
                data += str(chunk)

        fs.delete(filename)

        current_time = datetime.datetime.utcnow()
        list_filename = download_id + "_" + current_time.strftime('%Y%m%d_%H%M%S') + ".reload"

        task_result = tasks.run_reload(request.session['queue'], xml_filename, data, list_filename)

    except Exception as error:
        status = str(error)
        try:
            os.remove(filename)
        except Exception:
            pass

    else:

        if not task_result[0] or not task_result[1]:
            status = task_result[2]
            result = False
        elif task_result[2] == "0":
            status = 'no file found to redownload'
        elif task_result[2] == '1':
            status = 'one file found and marked  to redownload'
        else:
            status = '%s files found and marked  to redownload'.format(task_result[2])

    return JsonResponse({'error': False, 'result': result, 'message': status})


def copy_as_file(request, xml_filename, download_id):
    error = ''
    queue = request.session['queue']
    dst = request.POST['filename']
    download_id_dest = dst.replace(" ", "_")

    result = tasks.exists_configuration_id(queue, download_id_dest)
    if not result[0] or not result[1]:
        error = "Server error"
    elif result[2]:
        error = "Download id already exists"
    else:
        result = tasks.exists_configuration_files(queue, (dst + EXTENSION_ACTIVE, dst + EXTENSION_DISABLED))
        if not result[0] or not result[1]:
            error = "Server error"
        elif result[2]:
            error = "Filename already exists"

    if error == '':
        try:
            is_success = downloaderXml.copy_as_file(queue,
                                                    xml_filename, dst + EXTENSION_DISABLED, download_id_dest)
            if not is_success:
                error = "Copy error"
        except XMLSyntaxError as err:
            error = str(err)

        if error != '':
            return JsonResponse({'result': 'false', 'message': error})

        file_map, map_file_id, map_file_id_test, task_error = downloaderXml.get_downloads_with_error(queue)

        kwvars = {'file_map': file_map, 'map_file_id': map_file_id,
                  'tabSelected': 'desactivated', 'error': error,
                  'xml_file_name': dst + EXTENSION_DISABLED, 'event': 'filecopied' if not error else ''}
        return render(
            request,
            'adminview/activateddownload.html',
            kwvars,
        )
    else:
        return JsonResponse({'result': 'false', 'message': error})


def create_and_edit_download(request, download_id):
    download_file_name_disabled = download_id + EXTENSION_DISABLED

    downloaderXml.create_download(request.session['queue'], download_file_name_disabled)
    xml_filename = download_file_name_disabled

    return HttpResponseRedirect(reverse('dladmin:edit', args=(xml_filename,)))


def new_queue(request):
    queue = request.POST['new_queue']
    request.session['queue'] = queue

    if queue == '':
        file_map, map_file_id, map_file_id_test = {}, {}, {}
        text_error = ""
    else:
        file_map, map_file_id, map_file_id_test, text_error = downloaderXml.get_downloads_with_error(queue)

    return render(
        request,
        'adminview/activateddownload.html',
        {
            'file_map': file_map,
            'map_file_id': map_file_id,
            'map_file_id_test': map_file_id_test,
            'error': text_error,
            'file_name': ''
        },
    )


def help(request):
    dict_help = \
        {
            'id': _('id.help'),
            # edit
            # sourcee
            'type_remote': _('type_remote.help'),
            'server_remote': _('server_remote.help'),
            'port_remote': _('port_remote.help'),
            'user_remote': _('user_remote.help'),
            'passwd_remote': _('passwd_remote.help'),
            'repos_remote': _('repos_remote.help'),
            'protocol_option': _('protocol_option.help'),
            # date extraction
            'data_reader': _('data_reader.help'),
            'regexp_date': _('regexp_date.help'),
            'date_format': _('date_format.help'),
            'regexp_date2': _('regexp_date2.help'),
            'date_format2': _('date_format2.help'),
            # Selection
            'redownload': _('redownload.help'),
            'include_files_regexp': _('include_files_regexp.help'),
            'ignore_files_regexp': _('ignore_files_regexp.help'),
            'ignore_files_modify_older_than': _('ignore_files_modify_older_than.help'),
            'ignore_directories_regexp': _('ignore_directories_regexp.help'),
            'ignore_directories_newer_than': _('ignore_directories_newer_than.help'),
            'ignore_directories_older_than': _('ignore_directories_older_than.help'),
            'directories_pattern': _('directories_pattern.help'),
            'directories_delta': _('directories_delta.help'),
            'directories_maxdate': _('directories_maxdate.help'),
            'directories_mindate': _('directories_mindate.help'),
            'opensearch_dataset': _('opensearch_dataset.help'),
            'opensearch_area': _('opensearch_area.help'),
            'opensearch_format': _('opensearch_format.help'),
            'geotemporal_list': _('geotemporal_list.help'),
            # Destination
            'repos_local': _('repos_local.help'),
            'type_local': _('type_local.help'),
            'repos_local_tree': _('repos_local_tree.help'),
            'repos_local_SAFE': _('repos_local_SAFE.help'),
            'spool_location': _('spool_location.help'),
            # Post processing
            'checksum': _('checksum.help'),
            'compression_type': _('compression_type.help'),
            'compressionSubDir': _('compressionSubDir.help'),
            'extraction_type': _('extraction_type.help'),
            'extractionSubDir': _('extractionSubDir.help'),
            'validation': _('validation.help'),
            'links_path': _('links_path.help'),
            'file_group_name': _('file_group_name.help'),
            # Advanced
            'loop_delay': _('loop_delay.help'),
            'wait_before_download_delay': _('wait_before_download_delay.help'),
            'wait_between_downloads': _('wait_between_downloads.help'),
            'max_activated_files_by_loop': _('max_activated_files_by_loop.help'),
            'parallel': _('parallel.help'),
            'nb_retry': _('nb_retry.help'),
            'check_provider_before_download': _('check_provider_before_download.help'),
            'wait_between_scan': _('wait_between_scan.help'),
            'purge_scan_older_than': _('purge_scan_older_than.help'),
            'keep_last_scan': _('keep_last_scan.help'),
            'to_download_max_nbr': _('to_download_max_nbr.help'),
            'protocol_timeout': _('protocol_timeout.help'),
            'monitoring_enabled': _('monitoring_enabled.help'),
            'project_name': _('project_name.help'),
            'max_activated_flow': _('max_activated_flow.help'),
            'debug_enabled': _('debug_enabled.help')
        }

    return HttpResponse(dict_help)
