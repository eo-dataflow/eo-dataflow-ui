/**
 * Utils function for "django-downloader" plugin : add-on : function : - test of
 * regexp
 * 
 * @author : Randrianarisoa Haingonirina
 */

function getJSONParsedMap(source) {
	if (source != "") {
		var js_list = ((source).replace(/&(l|g|quo)t;/g, function(a, b) {
			return {
				l : '<',
				g : '>',
				quo : '"'
			}[b];
		}));

		js_list = js_list.replace(/u'/g, '\'');
		js_list = js_list.replace(/'/g, '\"');
		js_list = js_list.replace(/[\n]/gi, "");

		return JSON.parse(js_list);
	} else {
		return source
	}
}

function regexpValidation(file_name, regexpression) {
	// alert(file_name);
	// alert(regexpression);
	var error_exp_file = "bad_test";
	var error_exp_date = "no_date";

	var reg = new RegExp(regexpression, "g");
	var datereg = new RegExp("([0-9]{8})");

	if (reg.test(file_name)) {
		if (datereg.test(file_name)) {
			var dateconcat = file_name.match(datereg);
			return dateconcat[0];
		} else {
			return error_exp_date;
		}
	} else {
		return error_exp_file;
	}
}

function regexpcheckValid(regexpression) {
	var regular = new RegExp(regexpression);
	var bool = regular.compile(regexpression);
	return bool;
}

function isInteger(int_value) {
	var regexInt = new RegExp("^-{0,1}\\d*\\.{0,1}\\d+$");
	if (regexInt.test(int_value)) {
		return true;
	}
	return false;
}

/*
 * get date from a string like 19871002 using format YMD => 1987/10/02
 * 
 * @author Randrianarisoa Haingonirina
 */
function getDateFromFormattedString(returnvalue, format) {
	var year = "";
	var month = "";
	var day = "";

	// case 1 : Y M D
	if (format == "YMD") {
		year = returnvalue.substring(0, 4);
		month = returnvalue.substring(4, 6);
		day = returnvalue.substring(6, 8);
	} else if (format == "YDM") {
		year = returnvalue.substring(0, 4);
		day = returnvalue.substring(4, 6);
		month = returnvalue.substring(6, 8);
	} else if (format == "DMY") {
		day = returnvalue.substring(0, 2);
		month = returnvalue.substring(2, 4);
		year = returnvalue.substring(4, 8);
	} else if (format == "MDY") {
		month = returnvalue.substring(0, 2);
		day = returnvalue.substring(2, 4);
		year = returnvalue.substring(4, 8);
	} else if (format == "MYD") {
		month = returnvalue.substring(0, 2);
		year = returnvalue.substring(2, 6);
		day = returnvalue.substring(6, 8);
	} else if (format == "DYM") {
		month = returnvalue.substring(0, 2);
		year = returnvalue.substring(2, 6);
		day = returnvalue.substring(6, 8);
	} else {
		return new Date();
	}
	var temp = new Date(month + "/" + day + "/" + year);
	return temp;
}

function getDateMatchStrftime(date, strf) {

	if (date == getDateFromFormattedString(date, "YMD").strftime(strf)) {
		var datews = getDateFromFormattedString(date, "YMD");
		return getDateFromFormattedString(date, "YMD");
	} else if (date == getDateFromFormattedString(date, "DMY").strftime(strf)) {
		var datews = getDateFromFormattedString(date, "DMY");
		return getDateFromFormattedString(date, "DMY");
	} else if (date == getDateFromFormattedString(date, "MDY").strftime(strf)) {
		var datews = getDateFromFormattedString(date, "MDY");
		return getDateFromFormattedString(date, "MDY");
	} else if (date == getDateFromFormattedString(date, "YDM").strftime(strf)) {
		var datews = getDateFromFormattedString(date, "YDM");
		return getDateFromFormattedString(date, "YDM");
	} else if (date == getDateFromFormattedString(date, "DYM").strftime(strf)) {
		var datews = getDateFromFormattedString(date, "DYM");
		return getDateFromFormattedString(date, "DYM");
	} else if (date == getDateFromFormattedString(date, "MYD").strftime(strf)) {
		var datews = getDateFromFormattedString(date, "MYD");
		return getDateFromFormattedString(date, "MYD");
	} else {
		var datews = getDateFromFormattedString(date, "YMD");
		return getDateFromFormattedString(date, "YMD");
	}
}

function formatDateView(value) {
	var months = new Array(12);
	months[0] = "January";
	months[1] = "February";
	months[2] = "March";
	months[3] = "April";
	months[4] = "May";
	months[5] = "June";
	months[6] = "July";
	months[7] = "August";
	months[8] = "September";
	months[9] = "October";
	months[10] = "November";
	months[11] = "December";

	return value.getDate() + " " + months[value.getMonth()] + ", "
			+ value.getFullYear();
}

/*
 * manage context id pour pallier au nom de fichier avec les points
 */

function getContextId(prefix, fileId) {
	var contextId = "";
	for ( var key in mapFileId) {
		if (key == fileId) {
			contextId = mapFileId[key];
		}
	}

	if (contextId == "") {
		if (fileId === undefined) {

		} else {
			if (fileId.search(/\./g) != -1) {
				contextId = fileId;
				contextId = contextId.replace(/\./g, '_');
				mapFileId[fileId] = contextId;
			} else {
				contextId = fileId;
			}
		}
	}
	return prefix + contextId;
}

function getRealId(fileId) {
	var realId = "";
	for ( var key in mapFileId) {
		if (mapFileId[key] == fileId) {
			realId = key;
		}
	}

	if (realId == "") {
		return fileId;
	}
	return realId;
}

function getContextIdWithPrefix(fileId) {
	var contextId = "";
	for ( var key in mapFileId) {
		if (key == fileId) {
			contextId = mapFileId[key];
		}
	}

	if (contextId == "") {
		if (fileId === undefined) {
		} else {
			if (fileId.search(/\./g) != -1) {
				contextId = fileId;
				contextId = contextId.replace(/\./g, '_');
				mapFileId[fileId] = contextId;
			} else {
				contextId = fileId;
			}
		}
	}
	return contextId;
}

function isActiveFile(filename) {
	// alert("active : "+filename);
	if (filename.search(/test/g) == -1) {
		return true;
	}
	return false;
}

function isTestFile(filename) {
	// alert("test : "+filename);
	if (filename.search(/test/g) != -1) {
		return true;
	}
	return false;
}
