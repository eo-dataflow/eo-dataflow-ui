/**
 * Utils function for "django-downloader" monitoring
 * 
 * @author : Randrianarisoa Haingonirina
 */

function getJSONSimpleParse(source) {
	if (source != "") {
		var js_list = source;
		js_list = js_list.replace(/u'/g, '\'');
		js_list = js_list.replace(/'/g, '\"');
		js_list = js_list.replace(/[\n]/gi, "");

		return JSON.parse(js_list);
	} else {
		return source;
	}
}

function getJSONParse(source) {
	if (source != "") {
		var js_list = source;
		js_list = js_list.replace('OrderedDict(/g', '');
		js_list = js_list.replace(')])/g', ")]");
		alert("la source refactored : " + source);
		return JSON.parse(js_list);
	} else {
		return source;
	}
}

function getUseNumberDate(chiffre) {
	if (chiffre == "1") {
		return "01";
	} else if (chiffre == "2") {
		return "02";
	} else if (chiffre == "3") {
		return "03";
	} else if (chiffre == "4") {
		return "04";
	} else if (chiffre == "5") {
		return "05";
	} else if (chiffre == "6") {
		return "06";
	} else if (chiffre == "7") {
		return "07";
	} else if (chiffre == "8") {
		return "08";
	} else if (chiffre == "9") {
		return "09";
	} else {
		return chiffre;
	}
}

function getSize(map) {
	var index = 0;
	$.each(map, function(date, nbfile) {
		index = index + 1;
	});
	return index;
}

