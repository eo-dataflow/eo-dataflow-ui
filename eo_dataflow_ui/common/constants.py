import os


DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
DATE_FORMAT_FRONT = '%Y-%m-%d'

EXTENSION_ACTIVE = ".download.xml"
EXTENSION_DISABLED = ".download.xml.OFF"
EXTENSION_BAD = ".download.xml.BAD"
EXTENSION_JOB = ".job"
EXTENSION_LISTING = ".list"
EXTENSION_TEST = ".download.test.xml"



SYNCHRO_HISTORY = "/internal/synchro/history/"

LOGGER_DEFAULT_FILE_NAME = "download_"
LOGGER_SYNCHRO_FILE_NAME = "synchro_"
LOGGER_RELOAD_FILE_NAME = "reload_"

RABBITMQ_TIMEOUT = 10

EO_DATAFLOW_TASKS = 'eo_dataflow_manager.worker.celery_tasks.'

XSD_DIR = os.path.join(os.path.dirname(__file__), "../static/xsd")

