import os
import os.path
import shutil
import time

LOCK_SLEEP_TIME = 0.2
READ_BUFFER = 1024 * 1024  # 1Mo


class File(object):

    def __init__(self, name):
        self.__path, self.__name = os.path.split(os.path.abspath(name))
        self.__mode = None
        self.__fd = False
        self.__lockFile = None

    def open(self, mode="r"):
        """

        """
        # Check if the File is already open
        if self.isOpen():
            raise IOError("File (%s) already opened" % self.getName())

        # if open in read/append mode
        if mode[0] in ("r", "a"):
            if not self.exist():
                # If File doesn't exist, raise an OSError exception
                raise OSError("File (%s) doesn't exist," % self.getName() +
                              "can try to open it only in write mode")

        opener = open
        args = {'name': self.getName(), 'mode': mode}
        self.__fd = opener(*[], **args)
        # save the mode in __mode
        self.__mode = mode

    def isOpen(self):
        if self.__fd:
            return True
        else:
            return False

    def fileDesc(self):
        return self.__fd

    def getName(self, short=False, real=False):
        if short:
            return self.__name
        if real and self.isLink():
            readFile = os.readlink(os.path.join(self.__path, self.__name))
            if not os.path.isabs(readFile):
                readFile = os.path.join(self.__path, readFile)
            return os.path.abspath(readFile)
        return os.path.join(self.__path, self.__name)

    name = property(lambda self: self.getName(short=True), None, None,
                    "name of the file (short name)")

    def getPath(self):
        """Return the path of the L{File}"""
        return self.__path

    path = property(getPath, None, None,
                    "path where located the file")

    def copy(self, copyName):
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError("FileSystem operation on open file")
        if self.isLink():
            real = self.getRealFile()
            return real.link(copyName, symbolic=True)
        else:
            shutil.copy(self.getName(), copyName)
            return File(os.path.join(copyName, self.getName(short=True)))

    def exist(self):
        """Return True if the file exist
        @rtype: C{bool}
        """
        return os.path.isfile(self.getName())

    def isLink(self):
        """Return True if the file is a link
        @rtype: C{bool}
        """
        return os.path.islink(self.getName())

    def getSize(self):
        """Return the size of the L{File}

        @return: the size of the L{File}
        @rtype: C{int}

        @raise OSError: if the instance of L{File} doesn't exist.
        """
        try:
            return os.path.getsize(self.getName())
        except:
            raise OSError("File '%s' doesn't exist" % self.getName())

    def getTime(self):
        """Return the time of last modification of the L{File}

        @return: the time of last modification of the L{File}
        @rtype: C{int}

        @raise OSError: if the instance of L{File} doesn't exist.
        """
        try:
            return os.path.getmtime(self.getName())
        except:
            raise OSError("File '%s' doesn't exist" % self.getName())

    # Lock file management
    def isLock(self):
        return self.__lockFile is not None and self.__lockFile.exist()

    def removeLockFileExist(self, lockExt):
        lf = File(self.getName() + lockExt)
        if lf.exist():
            lf.remove()
            self.__lockFile = None

    def acquireLock(self, lockExt, lockvalid):
        if self.__lockFile is not None:
            raise RuntimeError("File is already locked with %s" %
                               str(self.__lockFile))

        lf = File(self.getName() + lockExt)
        if lf.exist():
            return False
        # Try to get the lock
        try:
            lf.open('w')
        except:
            return False
        lf.write(lockvalid)
        lf.close()
        time.sleep(LOCK_SLEEP_TIME)
        lf.open()
        line = lf.readline()
        lf.close()
        if line != lockvalid:
            return False

        self.__lockFile = lf
        return True

    def getLock(self):
        if self.__lockFile is None:
            raise RuntimeError(
                "File hasn't lock file (use acquireLock to get one)")
        return self.__lockFile

    def unLock(self):
        if self.__lockFile is None:
            raise RuntimeError("File isn't locked, can't unLock the File")
        # Check if already exist
        if self.__lockFile.exist():
            # Close to can remove
            self.__lockFile.close()
            self.__lockFile.remove()
        # Unlock the file
        self.__lockFile = None

    def resetLock(self):
        """unLock file in memory only (don't remove lock file on disk). To use carefully !
        Mainly use by Scheduler process, to free lock on files which are sent to processing, because
        AbstractprocessRunner removes the lock file (on disk) put by the scheduler when starting processing
        """
        if self.__lockFile is None:
            raise RuntimeError("File isn't locked, can't unLock the File")
        self.__lockFile = None

    # define __repr__ use to get a textual presentation of the object with print
    def __repr__(self):
        """use to get a textual presentation of the L{File} with print"""
        return "File named '%s'" % self.__name

    # define __str__ use to get a textual representation of the object with str()
    def __str__(self):
        """use to get a textual representation of the L{File} with str()"""
        return self.__repr__()

    # define __eq__ to set the equality of to File that have same file for target
    def __eq__(self, other_file):
        """set the equality of to File that have same file for target"""
        if isinstance(other_file, File):
            return self.getName() == other_file.getName()
        return False

    # define __hash__ to get the unique hash for an unique filename (with path)
    def __hash__(self):
        """get the unique hash for an unique filename (with path)"""
        return hash("%d%s" % (hash(File), self.getName()))

    # define the iterable capatibility of the File object
    def __iter__(self):
        """x.__iter__() <==> iter(x)"""
        if not self.isOpen():
            raise IOError("I/O operation on closed file")
        return self.__fd.__iter__()

    # Mapping/define properties of File object
    size = property(getSize, None, None,
                    "size of the L{File}")

    name = property(lambda self: self.__name, None, None,
                    "name of the L{File}")

    mode = property(lambda self: self.__mode, None, None,
                    "opening's mode of the L{File}")

    closed = property(lambda self: not self.isOpen(), None, None,
                      "equal C{True} if the L{File} is close, C{False} if open")

    def close(self):
        """close() -> None or (perhaps) an integer.  Close the file.

Sets data attribute .closed to True.  A closed file cannot be used for
further I/O operations.  close() may be called more than once without
error.  Some kinds of file objects (for example, opened by popen())
may return an exit status upon closing."""
        if self.__fd:
            self.__fd.close()
            self.__fd = None
            self.__mode = None

    def flush(self, *args):
        """flush() -> None.  Flush the internal I/O buffer."""
        if not self.isOpen():
            raise IOError("I/O operation on closed file")
        return self.__fd.flush(*args)

    def isatty(self, *args):
        """isatty() -> true or false.  True if the file is connected to a tty device."""
        if not self.isOpen():
            raise IOError("I/O operation on closed file")
        return self.__fd.isatty(*args)

    def read(self, *args):
        """read([size]) -> read at most size bytes, returned as a string.

If the size argument is negative or omitted, read until EOF is reached.
Notice that when in non-blocking mode, less data than what was requested
may be returned, even if no size parameter was given."""
        if not self.isOpen():
            raise IOError("I/O operation on closed file")
        return self.__fd.read(*args)

    def readline(self, *args):
        """readline([size]) -> next line from the file, as a string.

Retain newline.  A non-negative size argument limits the maximum
number of bytes to return (an incomplete line may be returned then).
Return an empty string at EOF."""
        if not self.isOpen():
            raise IOError("I/O operation on closed file")
        return self.__fd.readline(*args)

    def readlines(self, *args):
        """readlines([size]) -> list of strings, each a line from the file.

Call readline() repeatedly and return a list of the lines so read.
The optional size argument, if given, is an approximate bound on the
total number of bytes in the lines returned."""
        if not self.isOpen():
            raise IOError("I/O operation on closed file")
        return self.__fd.readlines(*args)

    def seek(self, *args):
        """seek(offset[, whence]) -> None.  Move to new file position.

Argument offset is a byte count.  Optional argument whence defaults to
0 (offset from start of file, offset should be >= 0); other values are 1
(move relative to current position, positive or negative), and 2 (move
relative to end of file, usually negative, although many platforms allow
seeking beyond the end of a file).  If the file is opened in text mode,
only offsets returned by tell() are legal.  Use of other offsets causes
undefined behavior.
Note that not all file objects are seekable."""
        if not self.isOpen():
            raise IOError("I/O operation on closed file")
        return self.__fd.seek(*args)

    def tell(self, *args):
        """tell() -> current file position, an integer (may be a long integer)."""
        if not self.isOpen():
            raise IOError("I/O operation onclosed file")
        return self.__fd.tell(*args)

    def write(self, *args):
        """write(str) -> None.  Write string str to file.

Note that due to buffering, flush() or close() may be needed before
the file on disk reflects the data written."""
        if not self.isOpen():
            raise IOError("I/O operation on closed file")
        return self.__fd.write(*args)

    def writelines(self, *args):
        """writelines(sequence_of_strings) -> None.  Write the strings to the file.

Note that newlines are not added.  The sequence can be any iterable object
producing strings. This is equivalent to calling write() for each string."""
        if not self.isOpen():
            raise IOError("I/O operation on closed file")
        return self.__fd.writelines(*args)

    def link(self, linkName, symbolic=False, keepExist=False):
        """Link the L{File} to a new place

        @param linkName: the link L{File} name
        @type linkName: C{str}
        @param symbolic: create a symbolic link instead of a hard link (default C{False})
        @type symbolic: C{bool}
        @param keepExist: If true, do not overwrite L{linkName} if exist (default C{False})
        @type keepExist: C{bool}
        @return: a L{File} instance of the link file
        @rtype: L{File}

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: if the instance of L{File} is actually open.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError("FileSystem operation on open file")

        if not keepExist and (os.path.isfile(linkName) or
                              os.path.islink(linkName)):
            os.remove(linkName)
        if symbolic:
            os.symlink(self.getName(), linkName)
        else:
            os.link(self.getName(), linkName)
        return File(linkName)

    def getRealFile(self):
        """Return the file pointed self (if self.isLink() == True)
        @rtype: C{File}
        """
        if not self.isLink():
            raise "File %s isn't a symbolic link, can't return real file" % self.getName()
        return File(self.getName(real=True))

    def remove(self):
        """Remove the L{File} from the disk

        @raise OSError: if the instance of L{File} doesn't exist.
        @raise IOError: if the instance of L{File} is actually open.
        """
        if not self.exist():
            raise OSError("File '%s' doesn't exist" % self.getName())
        if self.isOpen():
            raise IOError("FileSystem operation on open file")
        os.remove(self.getName())
