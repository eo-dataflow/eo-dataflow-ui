from celery.exceptions import TimeoutError
from django.conf import settings
from typing import Any, Optional, Tuple
from eo_dataflow_ui.common.constants import RABBITMQ_TIMEOUT, EO_DATAFLOW_TASKS

# DOWNLOADER_QUEUE = 'UI.server.1'
DOWNLOADER_QUEUE = ''
# -------------------------------------------------------------


def get_result(task: str, args: Tuple[Optional[Any]],
               queue: str, timeout: int = RABBITMQ_TIMEOUT) -> Tuple[bool, bool, Any]:
    try:
        result = settings.CELERY.send_task(
            EO_DATAFLOW_TASKS + task,
            args,
            queue=queue
        )
        #  print(f'task --> message : {result}, {task}, args : {args}, queue : {queue}, message : {result}')
        result.get(timeout=timeout)
        result_info = result.info
        #  print(f'task <-- message : {result}, {task}, info : {result_info})')
    except TimeoutError:
        return False, False, 'timeout_error'
    except Exception as err:
        return True, False, 'server_error: ' + str(err)
    else:
        if isinstance(result_info, list):
            if not result_info[0]:
                return True, False, 'server_error: ' + str(result_info[1])
            return True, result_info[0], result_info[1]
        return True, True, result_info
# -------------------------------------------------------------


def archive_download(queue: str, filename: str, work_id: str, suffix: str):
    return get_result('archive_download', (filename, work_id, suffix), queue)


# -------------------------------------------------------------


def get_configuration_files(queue: str):
    return get_result('get_configuration_files', (), queue)


def read_configuration_files_summary(queue: str):
    return get_result('read_configuration_files_summary', (), queue)


def read_configuration_file(queue: str, filename: str):
    return get_result('read_configuration_file', (filename, ), queue)


def rename_configuration_file(queue: str, filename: str, file_new: str):
    result_task = exists_configuration_file(queue, filename)
    if result_task[0] and result_task[1]:
        return get_result('rename_configuration_file', (filename, file_new), queue)
    return result_task


def copy_configuration_file(queue: str, filename: str, filepath: str):
    return get_result('copy_configuration_file', (filename, filepath), queue)


def remove_configuration_file(queue: str, filename: str):
    return get_result('remove_file', (filename, ), queue)


def write_configuration_file(queue: str, filename: str, content: str):
    return get_result('write_configuration_file', (filename, content), queue)


def exists_configuration_file(queue: str, filename: str):
    return get_result('exists_configuration_file', (filename,), queue)


def exists_configuration_files(queue: str, filenames: Tuple[str]):
    return get_result('exists_configuration_files', (filenames,), queue)


def exists_configuration_id(queue: str, download_id: str):
    return get_result('exists_configuration_id', (download_id, ), queue)


# -------------------------------------------------------------
# Testing
# -------------------------------------------------------------
def read_test_state_file(queue: str, filename: str):
    return get_result('read_test_state_file', (filename, ), queue)


# -------------------------------------------------------------
# Reloading
# -------------------------------------------------------------
def run_reload(queue: str, xml_filename: str, data: str, data_filename: str):
    return get_result('run_reload', (xml_filename, data, data_filename), queue)


# -------------------------------------------------------------
# Re-downloading errors
# -------------------------------------------------------------
def redownload_error(queue: str, xml_filename: str):
    return get_result('run_redownload', (xml_filename, ), queue)


def count_download_error(queue: str, xml_filename: str):
    return get_result('count_redownload', (xml_filename, ), queue)


# -------------------------------------------------------------
# Synchronization
# -------------------------------------------------------------
def check_status_synchro(queue: str, synchro_job_file: str):
    return get_result('check_status_synchro', (synchro_job_file, ), queue)


def run_synchro(download_id, queue: str, configuration_filename: str,
                listing_filename: str, synchro_job_file: str, synchro_subpath: str):
    return get_result('run_synchro',
                      (download_id, configuration_filename, listing_filename, synchro_job_file, synchro_subpath),
                      queue)


def read_synchro_state_file(queue: str, filename: str):
    return get_result('read_synchro_state_file', (filename, ), queue)


def read_log_file(queue: str, filename: str) -> Tuple[bool, bool, Any]:
    return get_result('read_log_file', (filename, ), queue)


def copy_listing_file(queue: str, filepath: str, work_id: str) -> Tuple[bool, bool, Any]:
    return get_result('copy_listing_file', (filepath, work_id), queue)


def write_manual_listing(queue: str, filename: str, work_id: str, content: str) -> Tuple[bool, bool, Any]:
    return get_result('write_manual_listing', (filename, work_id, content), queue)


def read_manual_listing(queue: str, filename: str, work_id: str) -> Tuple[bool, bool, Any]:
    return get_result('read_manual_listing', (filename, work_id), queue)


def read_listing_file(queue: str, full_filename: str) -> Tuple[bool, bool, Any]:
    return get_result('read_listing_file', (full_filename, ), queue)

# --------------------------------------------------------------------------------------


def rename_a_file(queue: str, filename: str, newfile: str) -> Tuple[bool, bool, Any]:
    return get_result('rename_file', (filename, newfile), queue)


def write_content_file(queue: str, filename: str, content: str) -> Tuple[bool, bool, Any]:
    return get_result('create_file', (filename, content), queue)


def copy_file(queue: str, filename: str, filepath: str) -> Tuple[bool, bool, Any]:
    return get_result('copy_file', (filename, filepath), queue)


def remove_file(queue: str, filename: str) -> Tuple[bool, bool, Any]:
    return get_result('remove_file', (filename, ), queue)


def ping(queue: str) -> Tuple[bool, bool, Any]:
    return get_result('ping', (), queue)
