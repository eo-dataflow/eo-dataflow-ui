

def get_workspace_dir_name(download_id):
    tmpName = ""
    for i in download_id:
        if i.isalnum() or i.isspace():
            tmpName = tmpName + i
        else:
            tmpName = tmpName + " "

    return "_".join(tmpName.split())
