# Django settings for eo-dataflow-ui project.
from __future__ import with_statement
from pkg_resources import resource_filename as pkg_file
from lxml import etree, objectify
import django
import os
from celery import Celery
import simplejson

PROJECT_ROOT = os.path.dirname(__file__)

# Get settings from environment variables
# -----------------------------------------------------------------------------
BROKER_URL = os.environ.get('BROKER_DSN')
MANAGEMENT_URL = os.environ.get('MANAGEMENT_DSN')
ALLOWED_HOST_URL = os.environ.get('ALLOWED_HOST_URL')
if os.environ.get('DJANGO_DEBUG', 'FALSE').upper() == 'TRUE':
    DJANGO_DEBUG = True
else:
    DJANGO_DEBUG = False

CELERY_CFG_FILE = os.environ.get('CELERY_CFG', os.path.join(PROJECT_ROOT, 'celery_cfg.json'))
if not os.path.exists(CELERY_CFG_FILE):
    raise Exception('Celery configuration file %s not found.' % CELERY_CFG_FILE)


URL_PREFIX = os.environ.get('URL_PREFIX', '')

DEBUG = DJANGO_DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': os.path.join(PROJECT_ROOT, 'db/downloader.db'),                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}


# Hosts/domain names that are valid for this site; required if DEBUG is False
ALLOWED_HOSTS = [ALLOWED_HOST_URL]

TIME_ZONE = 'UTC'

LANGUAGE_CODE = 'en'

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media') + '/'
MEDIA_URL = '/%smedia/' % URL_PREFIX

STATIC_ROOT = os.path.join(os.path.dirname(__file__), 'static') + '/'

STATIC_URL = '/%sstatic/' % URL_PREFIX

STATICFILES_DIRS = (
    pkg_file('eo_dataflow_ui', 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

LOCALE_PATHS = (
    os.path.join(STATIC_ROOT, 'locale'),
)
# Make this unique, and don't share it with anybody.
SECRET_KEY = 't25jsr7eh9!it09qhw4!^%zg)0&)l3#g^!pyj!#0w+^@2*3x9h'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                #'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                # ...
            ],
            #'builtins': ["debugtools.templatetags.debugtools_tags"],
        },
    },
]

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# Lien vers la page si l'authentification echoue
LOGIN_URL = '%saccount/login/' % URL_PREFIX

# Lien vers la page si l'authentification reussi
LOGIN_REDIRECT_URL = '%sdownloaderadmin/' % URL_PREFIX

ROOT_URLCONF = 'eo_dataflow_ui.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'eo_dataflow_ui.wsgi.application'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    #'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'eo_dataflow_ui',
    'eo_dataflow_ui.apps.downloader_admin',
    'mod_wsgi.server' if 'NEED_MODWSGI' in os.environ and os.environ.get('NEED_MODWSGI') == "YES" else 'eo_dataflow_ui.apps'
)

SESSION_ENGINE = "django.contrib.sessions.backends.db"

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# infile_path = os.path.join(APPDATA, 'controller/sc_path.xml')
# with open(infile_path, 'r') as infile:
#    paths = objectify.parse(infile).getroot()

# This loads the celery configuration from FELYX_CELERY_CFG file
# with open(os.environ['CELERY_CFG'], 'rb') as f:
with open(CELERY_CFG_FILE, 'rb') as f:
    CELERY_CONF = simplejson.loads(f.read())
    CELERY_CONF['broker_url'] = BROKER_URL
    CELERY_CONF['management_url'] = MANAGEMENT_URL
    CELERY = Celery()
    CELERY.conf.update(**CELERY_CONF)

