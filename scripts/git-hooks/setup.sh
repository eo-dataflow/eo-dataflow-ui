#!/bin/bash
set -Eeuo pipefail

trap "echo ERR trap fired!" ERR

HOOKS_SRC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
GIT_DIR="$HOOKS_SRC/../../.git"
PYLINT=$(command -v pylint)

echo "Pylint path: $PYLINT"

cp "$HOOKS_SRC/pre-commit" "$GIT_DIR/hooks/"
sed -i "s:{{PYLINT}}:$PYLINT:g" "$GIT_DIR/hooks/pre-commit"

echo "Git hooks setup"