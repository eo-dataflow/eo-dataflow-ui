#!/usr/bin/env bash
set -Eeuo pipefail

trap "echo ERR trap fired!" ERR

function clean() {
  echo "---------------------------------------------------------------"
  echo "Clean workspace"
  echo "---------------------------------------------------------------"
  python setup.py clean --all
  find dist -type f -delete
  find lib -type d -name "*.egg-info" -exec rm -rf {} +
}

#
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_ROOT_DIR="$(dirname "$DIR")"
echo "Goto $PROJECT_ROOT_DIR"
cd "$PROJECT_ROOT_DIR"

# clean ws
clean

# generate wheel
echo "---------------------------------------------------------------"
echo "Generate wheel"
echo "---------------------------------------------------------------"
python setup.py bdist_wheel

#
echo "---------------------------------------------------------------"
echo "Deploy to binary repository"
echo "---------------------------------------------------------------"

if [[ -z $(find dist -name "*.dev*") ]]; then
  echo "Upload to RELEASE"
  twine upload --repository nexus-public-release dist/*
else
  echo "Upload to SNAPSHOT"
  twine upload --repository nexus-public-snapshot dist/*
fi

# clean ws
clean
