FROM python:3.8.7-slim-buster

################################################################################
# MISE A JOUR DE L'OS
################################################################################

RUN rm -rf /var/lib/apt/lists/* && \
    apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y purge --auto-remove -o APT::AutoRemove::RecommendsImportant=false

################################################################################
# CREATION UTILISATEUR NON-PRIVILEGIE
################################################################################

RUN addgroup --system --gid 101 nginx && \
    adduser --system --disabled-login --ingroup nginx --no-create-home --home /nonexistent --gecos "nginx user" --shell /bin/false --uid 101 nginx

################################################################################
# INSTALLATION NGINX NON-PRIVILEGIE
################################################################################

RUN apt-get install --no-install-recommends --no-install-suggests -y gnupg2 ca-certificates && \
    NGINX_GPGKEY=573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62; \
    found=''; \
    for server in \
        ha.pool.sks-keyservers.net \
        hkp://keyserver.ubuntu.com:80 \
        hkp://p80.pool.sks-keyservers.net:80 \
        pgp.mit.edu \
    ; do \
        echo "Fetching GPG key $NGINX_GPGKEY from $server"; \
        apt-key adv --keyserver "$server" --keyserver-options timeout=10 --recv-keys "$NGINX_GPGKEY" && found=yes && break; \
    done; \
    test -z "$found" && echo >&2 "error: failed to fetch GPG key $NGINX_GPGKEY" && exit 1; \
    apt-get remove --purge --auto-remove -y gnupg1 && rm -rf /var/lib/apt/lists/* && \
    echo "deb http://nginx.org/packages/debian buster nginx" | tee /etc/apt/sources.list.d/nginx.list && \
    apt-get update && \
    apt-get -y install nginx=1.18.0-1~buster git && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

RUN chown -R 101:0 /var/cache/nginx && \
    chmod -R g+w /var/cache/nginx && \
    chown -R 101:0 /etc/nginx && \
    chmod -R g+w /etc/nginx

################################################################################
# DEPENDANCES PYTHON
################################################################################

RUN apt-get install --no-install-recommends --no-install-suggests -y \
    python-pip \
    build-essential gettext \
    procps iputils-ping telnet iptables sqlite3 libsqlite3-dev && \
    pip install --upgrade pip && \
    pip install uwsgi && \
    apt-get remove --purge --auto-remove -y \
    python-pip \
    build-essential

WORKDIR /opt

#RUN git clone https://gitlab.ifremer.fr/eo-dataflow/eo-dataflow-ui.git && rm -rf /var/cache/apk/*
ADD . /opt/eo-dataflow-ui

WORKDIR /opt/eo-dataflow-ui

RUN pip install -r /opt/eo-dataflow-ui/requirements.txt && \
    django-admin compilemessages && \
    pip install .

################################################################################
# RESSOURCES SUPPLEMENTAIRES
################################################################################

ADD "resources/bin/entrypoint.sh" /opt/entrypoint.sh
ADD "resources/nginx/nginx.conf" /tmp/nginx.conf
ADD "resources/nginx/downloader.conf" /tmp/downloader.conf
ADD "resources/nginx/uwsgi.ini" /opt/eo-dataflow-ui/conf/uwsgi.ini

RUN chgrp -R nginx /opt/entrypoint.sh && \
    chmod 555 /opt/entrypoint.sh && \
    cat /tmp/downloader.conf > /etc/nginx/conf.d/default.conf && \
    cat /tmp/nginx.conf > /etc/nginx/nginx.conf && \
    chmod 666 /opt/eo-dataflow-ui/conf/uwsgi.ini

#############################################################################
# CONFIGURATION DOWNLOADER
#############################################################################

ENV PWD=/opt/eo-dataflow-ui
ENV CELERY_CFG=/opt/eo-dataflow-ui/conf/celery_cfg.json
ENV RABBITMQ_HOST=""
ENV LOGTOFILE=NO
ENV MANAGEMENT_DSN=http://127.0.0.1:15672
ENV BROKER_DSN=amqp://guest:guest@127.0.0.1:5672/eo-dataflow-ui
ENV ALLOWED_HOST_URL="*"
ENV DJANGO_DEBUG=FALSE


RUN mkdir -p ${PWD}/conf/db && \
    export CELERY_CFG=${PWD}/conf/celery_cfg.json && \
    django-admin migrate --settings conf.settings --pythonpath $PWD && \
    chmod -R 777  ${PWD}/conf/db && \
    mkdir -p ${PWD}/conf/uwsgi && \
    chmod -R 777  ${PWD}/conf/uwsgi && \
    chmod 666 /etc/nginx/conf.d/default.conf && \
    mkdir -p ${PWD}/conf/media && \
    chmod 777 ${PWD}/conf/media && \
    mkdir /opt/logs && \
    chmod 777 /opt/logs 
    
VOLUME ["/opt/logs"]

#############################################################################
# PORTS POUVANT ETRE EXPOSES
#############################################################################

EXPOSE 8080/tcp

#############################################################################
# POINT D'ENTREE
#############################################################################

STOPSIGNAL SIGTERM

USER 101

WORKDIR /opt/eo-dataflow-ui/conf

ENTRYPOINT ["/bin/bash", "/opt/entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]

